# -*- coding: utf-8 -*-

import logging
import sys
import threading
import time
import getopt


import smpplib.gsm
import smpplib.client

from smpptest import sender
from wobble import slog, conf
from smpptest import receiver, utils

# if you want to know what's happening
logging.basicConfig(level='DEBUG')

config_file = 'conf/tester.conf'

pending_msgs = {}
msg_stats = {
    'count' : 0,
    'max_reply' : 0.0,
    'tot_reply' : 0.0,
    'max_queue' : 0
}
msg_lock = threading.Lock()

# Two parts, UCS2, SMS with UDH
#parts, encoding_flag, msg_type_flag = smpplib.gsm.make_parts(u'Привет мир!\n'*10)


def msg_sent(pdu):
    slog.info("sent ", pdu.message_id)

    msg = {'id': pdu.message_id, 'time': time.time()}
    msg_lock.acquire()
    pending_msgs[pdu.message_id] = msg
    msg_lock.release()

    slog.info("messages in queue ", len(pending_msgs))


def msg_acked(pdu):
    slog.info("received ", pdu.receipted_message_id)

    msg_lock.acquire()

    msg = pending_msgs.get(pdu.receipted_message_id)

    if msg is not None:
        re_time = time.time() - msg['time']
        slog.info("msg is ACK. Replied in ", re_time)
        del pending_msgs[pdu.receipted_message_id]
        queuesize  = len(pending_msgs)
        slog.info("messages in queue ", queuesize)
        msg_stats['count'] = msg_stats['count'] + 1
        msg_stats['tot_reply'] = msg_stats['tot_reply'] + re_time
        if re_time > msg_stats['max_reply']:
            msg_stats['max_reply'] = re_time
        if queuesize > msg_stats['max_queue']:
            msg_stats['max_queue'] = queuesize
    else:
        slog.info("msg is MO")
    msg_lock.release()


def main():
    global config_file

    try:
        (opts, args) = getopt.getopt(sys.argv[1:], "hc:D")
    except getopt.GetoptError:
        usage()
        sys.exit(1)
    debug_mode = None
    for (opt, val) in opts:
        if opt == "-h":
            usage()
            sys.exit()
        if opt == "-c":
            config_file = val
        if opt == "-D":
            debug_mode = True

    # read configuration file
    conf.read(config_file)
    slog.info("configuration", conf.get_all())

    # rearrange into list of dicts

    packet_rate = conf.get_float('CLIENT', 'packet_rate')

    if packet_rate == 0:
        slog.warn("Packet rate is zero, exiting")
        sys.exit(0)

    # set logging stream
    daemon = conf.get_bool("MAIN", "daemon")

    if daemon:
        log_file = conf.get_str("MAIN", "log_file")
        slog.set_stream(log_file)

    # set logging levels
    if debug_mode:
        log_levels = ['error', 'warning', 'info', 'verbose']
    else:
        log_levels = ['error', 'warning']
    slog.output_tags(log_levels)

    # daemonize process

    if daemon:
        tools.daemonize()

    smpp_host = conf.get_str("CLIENT", "smpp_host", "localhost")
    smpp_port = conf.get_int("CLIENT", "smpp_port", 2775)

    source_addr = conf.get_str("CLIENT", "src_address")
    dest_address_prefix = conf.get_str("CLIENT", "dest_address_prefix")
    dest_address_length = conf.get_int("CLIENT", "dest_address_length")
    system_id = conf.get_str("CLIENT", "system_id")
    password = conf.get_str("CLIENT", "password")

    content = "Hello world! Message number "
    dlr = conf.get_bool("CLIENT", "delivery_receipt", True)
    runonce = False
    max_packets = conf.get_int("MAIN", "max_packets")
    update_interval = 100

    client = smpplib.client.Client(smpp_host, smpp_port)

    client.set_message_sent_handler(msg_sent)
    client.set_message_received_handler(msg_acked)

    client.connect()
    client.bind_transceiver(system_id=system_id, password=password)

    # listen in thread
    rec = receiver.receiver(client)
    rec.start()

    pktid=0
    intid=0
    inttime=0
    sttime = time.time()
    while True:
        if runonce == True and pktid > 1:
            slog.warn("Stopping because run once is set")
            return
        tstamp = time.time() - sttime
        if pktid == 0:
            speed = 0
        else:
            speed = pktid / tstamp

        if speed > packet_rate:
            continue

        if pktid >= max_packets and max_packets != 0:
            slog.info("maximum number of packets sent, stopping")
            break
        if intid >= update_interval:
            intspeed = intid / (tstamp - inttime)
            inttime = tstamp
            intid = 0
            slog.warn(pktid, "of", max_packets, "done. Speed : ", intspeed, "requests/sec")
        slog.verb("preparing packet #", pktid, "time", tstamp)
        intid += 1

        msgtxt = content + str(pktid)

        if speed <= packet_rate:
            destination_addr = utils.gen_number(dest_address_length, dest_address_prefix)
            sender.send(client, source_addr, destination_addr, msgtxt, dlr)
            pktid += 1

    elapsed = time.time() - sttime
    speed = (max_packets-1) / elapsed
    time.sleep(1)
    while len(pending_msgs) > 0:
        pass

    rec.stop()

    slog.info("STATS", msg_stats)
    avg_reply = 0
    if msg_stats['count']:
        avg_reply = msg_stats['tot_reply'] / msg_stats['count']
    slog.info("AVG REPLY", avg_reply, "SEND TIME ELAPSED ", elapsed, " SEND SPEED", speed, "TPS")


def usage():
    print "./tester.py  [ -c configfile] [-h] [-d]"
    print " -c configfile - configuration file, default: ./conf/tester.conf"
    print " -D debug mode"
    print " -h this help"


main()
