import traceback
from smpplib import exceptions
from threading import Thread, Event
from wobble import slog


class receiver(Thread):
	def __init__(self, client):
		self.result = None
		self.client = client
		super(receiver, self).__init__()
		self._stop_event = Event()

	def run(self):
		while True:
			try:
				self.client.read_once()
			except exceptions.UnknownCommandError:
				slog.warn("Unknown Command Received")
				traceback.print_exc()
			if self.stopped():
				return

	def stop(self):
		self._stop_event.set()

	def stopped(self):
		return self._stop_event.is_set()




