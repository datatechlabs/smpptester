import random

def gen_number(digits, prefix=''):

    number = str(random.randint(1,9))

    for x in range(1, digits-len(prefix)):
        n = str(random.randint(0,9))
        number += n
    return prefix+number