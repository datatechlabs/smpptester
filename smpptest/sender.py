import smpplib.gsm
import smpplib.consts

def send(client, source_addr, destination_addr, content, dlr):

	parts, encoding_flag, msg_type_flag = smpplib.gsm.make_parts(content)

	for part in parts:
		pdu = client.send_message(
			source_addr_ton=smpplib.consts.SMPP_TON_INTL,
			source_addr_npi=smpplib.consts.SMPP_NPI_ISDN,
			# Make sure it is a byte string, not unicode:
			source_addr=source_addr,

			dest_addr_ton=smpplib.consts.SMPP_TON_INTL,
			dest_addr_npi=smpplib.consts.SMPP_NPI_ISDN,
			# Make sure thease two params are byte strings, not unicode:
			destination_addr=destination_addr,
			short_message=part,

			data_coding=encoding_flag,
			esm_class=msg_type_flag,
			registered_delivery=dlr,
		)
		#print(pdu.sequence)